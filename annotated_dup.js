var annotated_dup =
[
    [ "controllerTask", null, [
      [ "ControllerTask", "classcontrollerTask_1_1ControllerTask.html", "classcontrollerTask_1_1ControllerTask" ]
    ] ],
    [ "encoderDriver", null, [
      [ "EncoderDriver", "classencoderDriver_1_1EncoderDriver.html", "classencoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "feedback", null, [
      [ "Feedback", "classfeedback_1_1Feedback.html", "classfeedback_1_1Feedback" ]
    ] ],
    [ "mcp9808", null, [
      [ "MCP9808", "classmcp9808_1_1MCP9808.html", "classmcp9808_1_1MCP9808" ]
    ] ],
    [ "motorDriver", null, [
      [ "MotorDriver", "classmotorDriver_1_1MotorDriver.html", "classmotorDriver_1_1MotorDriver" ]
    ] ],
    [ "pid", null, [
      [ "PID", "classpid_1_1PID.html", "classpid_1_1PID" ]
    ] ],
    [ "touchPanelDriver", null, [
      [ "touchPanelDriver", "classtouchPanelDriver_1_1touchPanelDriver.html", "classtouchPanelDriver_1_1touchPanelDriver" ]
    ] ]
];