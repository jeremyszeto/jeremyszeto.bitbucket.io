/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 405: Mechatronics", "index.html", [
    [ "Introduction", "index.html#intro", null ],
    [ "Course Description", "index.html#course", null ],
    [ "Portfolio", "index.html#portfolio", null ],
    [ "Source Code Repository", "index.html#repo", null ],
    [ "Lab 0x01: Vendotron Finite State Machine", "lab0x01.html", [
      [ "Summary", "lab0x01.html#lab0x01_summary", null ],
      [ "Design Requirements", "lab0x01.html#lab0x01_design", null ],
      [ "User Inputs", "lab0x01.html#lab0x01_inputs", null ],
      [ "Vendotron State Transition Diagram", "lab0x01.html#lab0x01_diagram", null ],
      [ "Documentation", "lab0x01.html#lab0x01_documentation", null ],
      [ "Source Code", "lab0x01.html#lab0x01_source", null ]
    ] ],
    [ "HW 0x02: Term Project System Modeling", "hw0x02.html", [
      [ "Summary", "hw0x02.html#hw0x02_summary", null ]
    ] ],
    [ "Lab 0x02: Think Fast!", "lab0x02.html", [
      [ "Summary", "lab0x02.html#lab0x02_summary", null ],
      [ "Design Requirements", "lab0x02.html#lab0x02_design", null ],
      [ "Overview", "lab0x02.html#lab0x02_overview", null ],
      [ "Documentation", "lab0x02.html#lab0x02_documentation", null ],
      [ "Source Code", "lab0x02.html#lab0x02_source", null ]
    ] ],
    [ "Lab 0x03: Pushing the Right Buttons", "lab0x03.html", [
      [ "Summary", "lab0x03.html#lab0x03_summary", null ],
      [ "Design Requirements", "lab0x03.html#lab0x03_design", null ],
      [ "Results", "lab0x03.html#lab0x03_results", null ],
      [ "Documentation", "lab0x03.html#lab0x03_documentation", null ],
      [ "Source Code", "lab0x03.html#lab0x03_source", null ]
    ] ],
    [ "HW 0x04: Linearization and Simulation of System Model", "HW0x04.html", [
      [ "Source Code", "HW0x04.html#hw0x04_source", null ]
    ] ],
    [ "Lab 0x04: Hot or Not?", "lab0x04.html", [
      [ "Summary", "lab0x04.html#lab0x04_summary", null ],
      [ "I2C Communications", "lab0x04.html#sec_lab0x04_I2C", null ],
      [ "Design Requirements", "lab0x04.html#lab0x04_design", null ],
      [ "Results", "lab0x04.html#lab0x04_results", null ],
      [ "Documentation", "lab0x04.html#lab0x04_documentation", null ],
      [ "Source Code", "lab0x04.html#lab0x04_source", null ]
    ] ],
    [ "HW 0x05: Pole Placement (Full State Feedback)", "hw0x05.html", [
      [ "Source Code", "hw0x05.html#hw0x05_source", null ]
    ] ],
    [ "Lab 0xFF: Term Project", "lab0xff.html", [
      [ "Summary", "lab0xff.html#lab0xff_summary", null ],
      [ "Reading from a Resistive Touch Panel", "lab0xff.html#rtp", null ],
      [ "Driving DC Motors with PWM and H-Bridges", "lab0xff.html#pwm", null ],
      [ "Reading from Quadrature Encoders", "lab0xff.html#enc", null ],
      [ "Reading from an Inertial Measurement Unit", "lab0xff.html#imu", [
        [ "Reading from Inertial Measurement Unit", "lab0xff.html#sec_t3", null ]
      ] ],
      [ "Balancing the Ball", "lab0xff.html#ball", null ],
      [ "Results", "lab0xff.html#results", null ],
      [ "Video Demonstration", "lab0xff.html#demo", null ],
      [ "Documentation", "lab0xff.html#lab0xff_documentation", null ],
      [ "Source Code", "lab0xff.html#lab0xff_source", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"HW0x04.html",
"main_8py.html#a811b84fa25efd822c86d2b0b0f0fa959"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';