var thinkfast__b_8py =
[
    [ "press", "thinkfast__b_8py.html#ad70f402afe511cd26a3e98d13a9e7beb", null ],
    [ "toggle", "thinkfast__b_8py.html#af11c61f501555ea07ce9b3e76d7ef84c", null ],
    [ "avg", "thinkfast__b_8py.html#ad4d09024dec51a566c218b6ebaa07f1e", null ],
    [ "button", "thinkfast__b_8py.html#a29400796725697fa649635dbe3d2c68f", null ],
    [ "IC", "thinkfast__b_8py.html#a36ff5626886b168f17fd742537e82d03", null ],
    [ "ic_capture", "thinkfast__b_8py.html#a71aec488abeddd255da24a787eaa4ec1", null ],
    [ "LED", "thinkfast__b_8py.html#a9d5a53663f12c4150a44ec69d3eb0243", null ],
    [ "OC", "thinkfast__b_8py.html#a76fdfa2f95ee9e84f8631420632fd794", null ],
    [ "PB3", "thinkfast__b_8py.html#af1c4acb90760da58d0129dcb444dd97d", null ],
    [ "prev_comp", "thinkfast__b_8py.html#a5b988bfb60579ea4aaf121e84c95ba42", null ],
    [ "reaction", "thinkfast__b_8py.html#ab36d99c5975923c85cfe864c47d3275e", null ],
    [ "reactions", "thinkfast__b_8py.html#ada341be73f81f6c49e156840d91b5f78", null ],
    [ "timer", "thinkfast__b_8py.html#ab4fe6597bc444104c47a84bbbbc82879", null ]
];