var namespaces_dup =
[
    [ "controllerTask", null, [
      [ "ControllerTask", "classcontrollerTask_1_1ControllerTask.html", "classcontrollerTask_1_1ControllerTask" ]
    ] ],
    [ "encoderDriver", null, [
      [ "EncoderDriver", "classencoderDriver_1_1EncoderDriver.html", "classencoderDriver_1_1EncoderDriver" ],
      [ "E1", "encoderDriver_8py.html#ac968f5c230e94c0ee400d66ed145f360", null ],
      [ "E2", "encoderDriver_8py.html#a12258d6d84660a22803881a2ec0330d0", null ],
      [ "interval", "encoderDriver_8py.html#a7a00c90ba28a2a34eb99e31cd87451be", null ],
      [ "next_time", "encoderDriver_8py.html#a84a6c8d62ef5397479e0cb95a7a46f0e", null ],
      [ "start_time", "encoderDriver_8py.html#a51d9e62d2412a7ab9fe8900a16b2ecf4", null ],
      [ "tim4", "encoderDriver_8py.html#a4168eaf668a428cb063b2b4936194e66", null ],
      [ "tim8", "encoderDriver_8py.html#a9a0c8a81be3af5fa423e7993dc64f62b", null ],
      [ "W1", "encoderDriver_8py.html#a2da7bc39f0345e0254a5d016c4b7246e", null ],
      [ "W2", "encoderDriver_8py.html#adbf582def677ab6b1ffaf69bda9cc0aa", null ]
    ] ],
    [ "feedback", null, [
      [ "Feedback", "classfeedback_1_1Feedback.html", "classfeedback_1_1Feedback" ]
    ] ],
    [ "Lab0x01", "namespaceLab0x01.html", null ],
    [ "Lab0x02", "namespaceLab0x02.html", null ],
    [ "Lab0x03", "namespaceLab0x03.html", null ],
    [ "Lab0x04", "namespaceLab0x04.html", null ],
    [ "Lab0xFF", "namespaceLab0xFF.html", null ],
    [ "main", null, [
      [ "center", "main_8py.html#a608e7e5680b97ceb8c873abf1b8d9142", null ],
      [ "E1", "main_8py.html#a47bc276b248f4443facc2d71d713e926", null ],
      [ "E2", "main_8py.html#a33f67108486f5e7cdfd98b135dec1c42", null ],
      [ "F1", "main_8py.html#a23a002d4dd32a862a91b7b3219af9559", null ],
      [ "F2", "main_8py.html#a10b71b5796dba47ba65c07ee4bf00de0", null ],
      [ "i2c", "main_8py.html#ab2bba10cbf0946121616503df3750664", null ],
      [ "imu", "main_8py.html#ae701983592736ef095bf138d981fc12a", null ],
      [ "interval", "main_8py.html#a86060447ac22c1b649ef0497e8adf1ba", null ],
      [ "k1", "main_8py.html#a71891f33a3060cf6f2a6ce06b9b6ca50", null ],
      [ "k2", "main_8py.html#ac398d749e55514d6b32a805d9680417d", null ],
      [ "k3", "main_8py.html#a9c59b28a9d085c06747b47563d856abf", null ],
      [ "k4", "main_8py.html#a537e722e9026bbcdb872a13d498b4cf6", null ],
      [ "k_ctrl", "main_8py.html#a061cc7ef26a806e6eb028c9651eb4dba", null ],
      [ "k_m", "main_8py.html#acf41a39ac0de650e5a8faa16c4c0195f", null ],
      [ "M1", "main_8py.html#a73ee3378bc5dfdb93d72d6f21190b8f9", null ],
      [ "M2", "main_8py.html#a727f0cc19c09f86d410ded57bcaf8307", null ],
      [ "system", "main_8py.html#a764a11ca8ba47f599feec76e83141d89", null ],
      [ "tim3", "main_8py.html#ac4facb646133471163e50a65749e8cb8", null ],
      [ "tim4", "main_8py.html#a53ed0320302e49d0f03d3eafd9f88dd8", null ],
      [ "tim8", "main_8py.html#aff91f87347a72a84bde0a3cb2fb19ed2", null ],
      [ "tp", "main_8py.html#ad0289199bb2ae74f6e368683aeeac254", null ],
      [ "x_count", "main_8py.html#a811b84fa25efd822c86d2b0b0f0fa959", null ],
      [ "x_len", "main_8py.html#afdeb37e345a4d9affb8d591893039733", null ],
      [ "y_count", "main_8py.html#aeb1cddad0c9793336a7e893bd84be464", null ],
      [ "y_len", "main_8py.html#a0b59383959024972a72a5d668c036a33", null ]
    ] ],
    [ "main0x03", null, [
      [ "callback", "main0x03_8py.html#a62a7c897fc854d840114c623a7755d02", null ],
      [ "run", "main0x03_8py.html#a1532c8497390567d537fbe8997c524d5", null ],
      [ "ADC", "main0x03_8py.html#a70dfa8d9647eb4e9423541f08927629d", null ],
      [ "button", "main0x03_8py.html#a275afc4c6234c62d1e128575aff8a3f9", null ],
      [ "extint", "main0x03_8py.html#a9e2c5b3f0813a60d449d111e691090e5", null ],
      [ "pressed", "main0x03_8py.html#ae8a0811e3f2563d18e07c4506fbf320b", null ],
      [ "uart", "main0x03_8py.html#a53f8b4e78034d4cd3ef7d471fc828902", null ]
    ] ],
    [ "main0x04", null, [
      [ "adc", "main0x04_8py.html#a34cee00fbde704dec088ad2919bc59e2", null ],
      [ "end", "main0x04_8py.html#a48a9c070d7d724c8bdd2f339fbefe80e", null ],
      [ "i2c", "main0x04_8py.html#a5fee292e70571b497f170d37296f2a07", null ],
      [ "mcp9808_temp", "main0x04_8py.html#aeca30f7dff3280c1b23b3d6b31857dc0", null ],
      [ "sensor", "main0x04_8py.html#ad7e638c484fd00fe0800641c11cb8082", null ],
      [ "start", "main0x04_8py.html#a9c2bd52034c9c419a8a80eb4a43c8ff5", null ],
      [ "stm32_temp", "main0x04_8py.html#a0c205da49ab560413dd6721c86b5be83", null ],
      [ "time", "main0x04_8py.html#a1b0335c0ed92d06a3f2c4fa276c1e57f", null ]
    ] ],
    [ "mcp9808", null, [
      [ "MCP9808", "classmcp9808_1_1MCP9808.html", "classmcp9808_1_1MCP9808" ],
      [ "i2c", "mcp9808_8py.html#ae99e487519ec535e9356de31b7f1dee7", null ],
      [ "sensor", "mcp9808_8py.html#a1fb9fbb0a6eefb380fcf6f842297e41e", null ]
    ] ],
    [ "motorDriver", null, [
      [ "MotorDriver", "classmotorDriver_1_1MotorDriver.html", "classmotorDriver_1_1MotorDriver" ],
      [ "duty", "motorDriver_8py.html#a3369674d93ab821fe5dd9b0ff654511d", null ],
      [ "M1", "motorDriver_8py.html#aec418804e3e068a23fc53a73619ff91a", null ],
      [ "M1_CH1", "motorDriver_8py.html#a2f096630e97b3a32546478778ccc9ad5", null ],
      [ "M1_CH2", "motorDriver_8py.html#a51067d4e9302ae86a6628f040657057a", null ],
      [ "M2", "motorDriver_8py.html#ae6a6b653dcbc9b05795d203aaa74df0f", null ],
      [ "M2_CH1", "motorDriver_8py.html#a96c82d7a1a85cd0e4e38b4186faa9acc", null ],
      [ "M2_CH2", "motorDriver_8py.html#a7f5b3127eb59b3b5dbb8d65d097bf413", null ],
      [ "pin_nFAULT", "motorDriver_8py.html#a87b9d2e4e83796a29d3d7f98ce403b57", null ],
      [ "pin_nSLEEP", "motorDriver_8py.html#a1bbbfd5466218daf2ff14092a1c26c11", null ],
      [ "tim3", "motorDriver_8py.html#ad53b0274ea70fe472ffdabb1ac3fef3d", null ]
    ] ],
    [ "pid", null, [
      [ "PID", "classpid_1_1PID.html", "classpid_1_1PID" ]
    ] ],
    [ "thinkfast_a", null, [
      [ "isr", "thinkfast__a_8py.html#a8e313aabe3601bb346d61d6c9133d025", null ],
      [ "avg", "thinkfast__a_8py.html#a30d31d92a7a95ff2c0efec13c7a21c8c", null ],
      [ "button", "thinkfast__a_8py.html#a67143aef955c5f2096eb8c7079b60b27", null ],
      [ "extint", "thinkfast__a_8py.html#a813e8c44b4b315256e16242eda3e23a0", null ],
      [ "LED", "thinkfast__a_8py.html#af5f7951da6b479bc3638371b16e6fd89", null ],
      [ "pressed", "thinkfast__a_8py.html#af3ed114651b993414eb3c749271c1889", null ],
      [ "react", "thinkfast__a_8py.html#a2a9ab0ef2e3ba20bb89b90dd680bf2bb", null ],
      [ "reactions", "thinkfast__a_8py.html#a44d21ac2bb6bec49dfb0d9f5df5a9e2e", null ],
      [ "start", "thinkfast__a_8py.html#a9368dcd8dadd22423cc7fa6ed8aed74c", null ]
    ] ],
    [ "thinkfast_b", null, [
      [ "press", "thinkfast__b_8py.html#ad70f402afe511cd26a3e98d13a9e7beb", null ],
      [ "toggle", "thinkfast__b_8py.html#af11c61f501555ea07ce9b3e76d7ef84c", null ],
      [ "avg", "thinkfast__b_8py.html#ad4d09024dec51a566c218b6ebaa07f1e", null ],
      [ "button", "thinkfast__b_8py.html#a29400796725697fa649635dbe3d2c68f", null ],
      [ "IC", "thinkfast__b_8py.html#a36ff5626886b168f17fd742537e82d03", null ],
      [ "ic_capture", "thinkfast__b_8py.html#a71aec488abeddd255da24a787eaa4ec1", null ],
      [ "LED", "thinkfast__b_8py.html#a9d5a53663f12c4150a44ec69d3eb0243", null ],
      [ "OC", "thinkfast__b_8py.html#a76fdfa2f95ee9e84f8631420632fd794", null ],
      [ "PB3", "thinkfast__b_8py.html#af1c4acb90760da58d0129dcb444dd97d", null ],
      [ "prev_comp", "thinkfast__b_8py.html#a5b988bfb60579ea4aaf121e84c95ba42", null ],
      [ "reaction", "thinkfast__b_8py.html#ab36d99c5975923c85cfe864c47d3275e", null ],
      [ "reactions", "thinkfast__b_8py.html#ada341be73f81f6c49e156840d91b5f78", null ],
      [ "timer", "thinkfast__b_8py.html#ab4fe6597bc444104c47a84bbbbc82879", null ]
    ] ],
    [ "touchPanelDriver", null, [
      [ "touchPanelDriver", "classtouchPanelDriver_1_1touchPanelDriver.html", "classtouchPanelDriver_1_1touchPanelDriver" ],
      [ "center", "touchPanelDriver_8py.html#a34c4a05cf80fe09f104202d8f6ba2a5a", null ],
      [ "data", "touchPanelDriver_8py.html#abcc73277248d70feed7243e70cc06a32", null ],
      [ "end", "touchPanelDriver_8py.html#a52da3432f335e3ff572883c1cbb9a0f7", null ],
      [ "PA0", "touchPanelDriver_8py.html#a3a4611de226fbb1ad737ad3c18239e17", null ],
      [ "PA1", "touchPanelDriver_8py.html#af2d5006de496e92242477438d3b3570c", null ],
      [ "PA6", "touchPanelDriver_8py.html#a82114f75f5a04738ca46d37d6a37fb8c", null ],
      [ "PA7", "touchPanelDriver_8py.html#aca37ad498389a5761a97d6022240805d", null ],
      [ "start", "touchPanelDriver_8py.html#a84df421b26cc36e12224a4ffe9eaf65b", null ],
      [ "task", "touchPanelDriver_8py.html#a26b56a31ac9c427b1415e676eacdae4e", null ],
      [ "x_count", "touchPanelDriver_8py.html#a9fd94a5b48fdff9e816e960130c5e3a7", null ],
      [ "x_len", "touchPanelDriver_8py.html#a666d1eeaf04ee9e117ad41639c25263d", null ],
      [ "y_count", "touchPanelDriver_8py.html#ab841b343cc59e8ed9838571bd6103d72", null ],
      [ "y_len", "touchPanelDriver_8py.html#ae1b2d6681a0061361d6063370be509a8", null ]
    ] ],
    [ "ui", null, [
      [ "export", "ui_8py.html#ae3f1323478f5196947ebc7c1e826dff6", null ],
      [ "kb_cb", "ui_8py.html#afbe49b25831596c94cacf4ffbc642cbc", null ],
      [ "run", "ui_8py.html#a66f609c358bf8db3d790302dbc0e8e9e", null ],
      [ "last_key", "ui_8py.html#a7913ab4bd226ee9486524f61e8fe7f88", null ],
      [ "ser", "ui_8py.html#a88b09884824cde8a2a647e4a1916b2e1", null ],
      [ "times", "ui_8py.html#a0e0612861258c3458bbf22d19e5209bd", null ],
      [ "values", "ui_8py.html#aa2cd095e7a36df10278b0b54ffded96f", null ]
    ] ],
    [ "vendotron", null, [
      [ "getBalance", "vendotron_8py.html#a0dacdac43e1daf8d10428b1e63f55cdf", null ],
      [ "getChange", "vendotron_8py.html#a868db430e9633154da2369c2110dd090", null ],
      [ "keystroke", "vendotron_8py.html#a950afa470b0b93cc97f3f063011d4223", null ],
      [ "VendotronTask", "vendotron_8py.html#aa20dd215f18f9ff505d4e87284b2fbfb", null ],
      [ "last_key", "vendotron_8py.html#a959d9c7dd7329e2aeb8e7a823db5bad3", null ],
      [ "vendo", "vendotron_8py.html#a137f11b7591756506a349f7efaf147f0", null ]
    ] ]
];