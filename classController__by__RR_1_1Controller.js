var classController__by__RR_1_1Controller =
[
    [ "__init__", "classController__by__RR_1_1Controller.html#a4758387c8fd4c6d7e73456f8f3a02e8a", null ],
    [ "correct_PWM", "classController__by__RR_1_1Controller.html#a9aedff3c2c16ff2670883feef50ca999", null ],
    [ "get_StateVar", "classController__by__RR_1_1Controller.html#a180e0f599631883aa4e14fbc28327653", null ],
    [ "run", "classController__by__RR_1_1Controller.html#a50b4afb0e84a97c23f7a1306a57e9579", null ],
    [ "euler", "classController__by__RR_1_1Controller.html#af493a0225976ad473c44864f65f6e1d2", null ],
    [ "gains1", "classController__by__RR_1_1Controller.html#affe5407c01cefe1ac8e2d7f1225c9a63", null ],
    [ "gains2", "classController__by__RR_1_1Controller.html#a60946219618b99c94b9dfb08e6261297", null ],
    [ "imu", "classController__by__RR_1_1Controller.html#aca8dbc77645381fa38c0cd507979eeb4", null ],
    [ "interval", "classController__by__RR_1_1Controller.html#a90ae59b61e158ccd22a8bef97b802513", null ],
    [ "L_1", "classController__by__RR_1_1Controller.html#ab5efec245ab0630a813b194b3f190f58", null ],
    [ "L_2", "classController__by__RR_1_1Controller.html#a8a9b28f36380b9ba18f24503a5b9b5cd", null ],
    [ "motor1", "classController__by__RR_1_1Controller.html#a8630365853a86f9b42548f5b3d4dbde6", null ],
    [ "motor2", "classController__by__RR_1_1Controller.html#a6008cbc2c7930c4d4059cb8f89e65234", null ],
    [ "scc", "classController__by__RR_1_1Controller.html#a1807b17b5f3c7a39bb2ddeb1954d6816", null ],
    [ "theta_dotx", "classController__by__RR_1_1Controller.html#ae8a577373f957ac103e0ffa2dbab1809", null ],
    [ "theta_doty", "classController__by__RR_1_1Controller.html#a1edc18572f75ce4e2ef1a17d843e704b", null ],
    [ "theta_x", "classController__by__RR_1_1Controller.html#a5641ee6f219a19d4c4c3440ab5725222", null ],
    [ "theta_xF", "classController__by__RR_1_1Controller.html#a3831dbb99587a248af8f96d3410b72ac", null ],
    [ "theta_y", "classController__by__RR_1_1Controller.html#a385621187a5ec3d829557159e8601853", null ],
    [ "theta_yF", "classController__by__RR_1_1Controller.html#aeed6ee0743ec1eb8bc953f4fca9ab3a1", null ],
    [ "Tm_M1", "classController__by__RR_1_1Controller.html#ac1faa194eba20883439bbfb575208372", null ],
    [ "Tm_M2", "classController__by__RR_1_1Controller.html#a4a6a2cadd64b8d21dd23db72ed0172d5", null ],
    [ "x", "classController__by__RR_1_1Controller.html#af8c6539f1abcbb352dc32f851d53f486", null ],
    [ "x_dot", "classController__by__RR_1_1Controller.html#a1758cccb0744270aed4ff12eef8597c5", null ],
    [ "xF", "classController__by__RR_1_1Controller.html#ad328076fe17bcd2b77805f8d45de146b", null ],
    [ "xyz", "classController__by__RR_1_1Controller.html#adcdc253934a10a8b02a1df7cc898b490", null ],
    [ "y", "classController__by__RR_1_1Controller.html#a7784d86a5386280ce45bcafefba0a391", null ],
    [ "y_dot", "classController__by__RR_1_1Controller.html#afbd193fbbda59e820bdde4d01b3b7673", null ],
    [ "yF", "classController__by__RR_1_1Controller.html#a8af06a56799c7c9de41164f0508db86b", null ]
];