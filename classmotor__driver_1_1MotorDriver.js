var classmotor__driver_1_1MotorDriver =
[
    [ "__init__", "classmotor__driver_1_1MotorDriver.html#a693163612f9f24481766a5957b54add3", null ],
    [ "callback", "classmotor__driver_1_1MotorDriver.html#aab41e8bc0cb398c656f76a37f9b5694b", null ],
    [ "check_fault", "classmotor__driver_1_1MotorDriver.html#acc4aae705d969a61502295fb73cc5aab", null ],
    [ "disable", "classmotor__driver_1_1MotorDriver.html#ad6e5414ca07a076c435af9a8b5c45d7c", null ],
    [ "enable", "classmotor__driver_1_1MotorDriver.html#a63b10626ef7cb4e9f2a84f6e659c196c", null ],
    [ "set_duty", "classmotor__driver_1_1MotorDriver.html#a8083024eb652783793e6742cb52f9fe5", null ],
    [ "extint", "classmotor__driver_1_1MotorDriver.html#a1a33025e69584caa375bb2d2f9bdfd26", null ],
    [ "fault", "classmotor__driver_1_1MotorDriver.html#a94bc694387152abc1d7d2cfc9288817c", null ],
    [ "pin_nFAULT", "classmotor__driver_1_1MotorDriver.html#a467074133d31fe6f7ff5f381f99dce9a", null ],
    [ "pin_nSLEEP", "classmotor__driver_1_1MotorDriver.html#acb701efb378aa775d60e73cfdbca8335", null ],
    [ "timer_ch1", "classmotor__driver_1_1MotorDriver.html#a7fcf934549403b547b31802d2c445108", null ],
    [ "timer_ch2", "classmotor__driver_1_1MotorDriver.html#a6e7ea7984fa8a69cc61370128da05939", null ]
];